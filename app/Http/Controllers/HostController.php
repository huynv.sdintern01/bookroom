<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\BookingModel;
use App\Models\ProvinceCity;
use App\Models\ProvinceCityModel;
use App\Models\Room;
use App\Models\RoomModel;
use Illuminate\Http\Request;

class HostController extends Controller
{
    private $room;
    private $province;
    private $booking;
    public function __construct()
    {
        $this->room = new RoomModel();
        $this->booking = new BookingModel();
        $this->province = new ProvinceCityModel();
    }

    //Di chuyển vào trang chủ của user chủ trọ
    public function indexHost()
    {
        $province = ProvinceCityModel::all();
        return view('hosts.index', compact('province'));
    }

    //Lấy danh sách các phòng của user này đăng lên mà đang được người khác đặt
    public function showListBooking()
    {
        $listRoom = $this->booking->getRoomBooking();
        return view('hosts.show_room_booking', compact('listRoom'));
    }

    //Chủ trọ lấy thông tin phòng mình đăng lên
    public function showListRoom()
    {
        $listRoom = $this->room->getDetailRoomByUser();
        return view('hosts.show_room_post', compact('listRoom'));
    }

    //Tạo một bài đăng mới
    public function storePost(PostRequest $request)
    { 
        $route = $this->room->addNewRoom($request);
        return redirect()->route($route)->with('success', 'Đăng bài thành công');
    }

    //Chủ trọ xác nhận cho thuê phòng
    public function updateAcceptBooking(Request $request)
    {
        $this->booking->updateStatusBooking($request);
        return redirect()->route('list.booking.room');
    }

    //Xóa bài đăng
    public function destroyRoom(Request $request)
    {
        $this->room->deleteRoom($request);
        return redirect()->route('list.room.post')->with('success', 'Xóa bài đăng thành công');
    }

    //Lấy thông tin phòng cần update
    public function editRoom(Request $request)
    {
        $detailRoom = $this->room->getDetailRoom($request);
        $province = ProvinceCityModel::all();
        return view('hosts.edit_room', compact(['detailRoom', 'province']));
    }

    public function updateRoom(Request $request)
    {
        //dd($request->toArray());
        $route = $this->room->updateRoom($request);
        return redirect()->route($route)->with('success', 'Cập nhật bài đăng thành công');
    }
}