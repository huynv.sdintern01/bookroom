<?php

namespace App\Http\Controllers;

use App\Models\BookingModel;
use App\Models\CoinModel;
use App\Models\PaymentModel;
use App\Models\ProvinceCityModel;
use App\Models\RoomModel;
use Illuminate\Http\Request;

class TenantController extends Controller
{
    private $room;
    private $listProvince;
    private $booking;
    private $payment;
    private $coin;
    public function __construct()
    {
        $this->room = new RoomModel();
        $this->booking = new BookingModel();
        $this->payment = new PaymentModel();
        $this->coin = new CoinModel();
        $this->listProvince = ProvinceCityModel::all();
    }

    //Di chuyển vào trang chủ của user đi thuê phòng
    public function indexTenant()
    {   
        return view('tenants.index',
        [ 
            'province' => $this->listProvince,
        ]);
    }

    //Lấy thông tin chi tiết của phòng
    public function showDetailRoom(Request $request)
    {
        $detailRoom = $this->room->getDetailRoom($request);
        return view('tenants.room_detail', compact('detailRoom'));
    }

    //Danh sách các phòng đã đặt có thể thanh toán
    public function showPaymentRoom()
    {
        $listRoomNeedPayment = $this->booking->getRoomNeedPayment();
        return view('tenants.room_need_payment', compact('listRoomNeedPayment'));
    }

    //filter room theo điều kiện
    public function filterRoom(Request $request)
    {
        $listRoom = $this->room->filter($request);
        // dd($listRoom);
        // dd(empty( $listRoom));
        return view('tenants.filter_result',[ 
            'province' => $this->listProvince,
            'listRoom' => $listRoom,
        ]);
    }
    
    //Đặt phòng
    public function bookingRoom(Request $request)
    {
        $this->booking->storeBooking($request);
        return redirect()->route('tenant.index')->with('success', 'Đặt phòng thành công');
    }

    //Trả về kết quả thanh toán và cập nhật thông tin thanh toán vào database: bảng payments, booking và coins
    public function returnPay(Request $request)
    {
        $this->coin->updateCoin($request);
        $this->booking->updateStatusBookingPaid($request);
        $this->payment->createPayment($request);
        
        return view('tenants.return_payment')->with('success', 'Thanh toán thành công');
    }
}