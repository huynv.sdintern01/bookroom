<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'tittle' => 'required|max:191',
            'content' => 'required|max:2000',
            'area' => 'required|numeric',
            'cost' => 'required|numeric|min:7',
            'file.*' => 'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'province_id' => 'required|numeric',
            'town_id' => 'required|numeric',
            'village_id' => 'required|numeric',
        ];
    }
}