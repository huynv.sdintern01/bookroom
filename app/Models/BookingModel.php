<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BookingModel extends Model
{
    use HasFactory;
    protected $table = "bookings";
    protected $fillable =['id_user', 'id_room', 'status', 'id_host'];

    //Relationship
    public function room()
    {
        return $this->belongsTo(RoomModel::class, 'id_room', 'id');
    }

    public function user()
    {
        return $this->belongsTo(UserModel::class, 'id_user', 'id');
    }

    //Thêm dữ liệu người đặt phòng và phòng được đặt vào database
    public function storeBooking($request)
    {
        BookingModel::create([
            'id_user' => Auth::id(), 
            'id_room' => $request->room_id, 
            'status' => "Booking",
            'id_host' => $request->id_host, 
        ]);
    }

    //Lấy ra các phòng đã được đặt
    public function getRoomBooking()
    {
        return BookingModel::with('room.image', 'user')->where('id_host', Auth::id())
                                                        ->where('status', 'Booking')->get()->toArray();
    }

    //Cập nhật lại status Booking -> Accepted
    public function updateStatusBooking($request)
    {
        BookingModel::where('id', $request->id)->update(['status' => 'Accepted']);
    }

    //Cập nhật ststus Accpeted -> Paid
    public function updateStatusBookingPaid($request)
    {
        $arrOrderInfor = explode("-", $request->vnp_OrderInfo);
        $arrTxnRef = explode("-", $request->vnp_TxnRef);
        $id_room = array_pop($arrTxnRef);
        $id_host = array_pop($arrOrderInfor);
        BookingModel::where('id_host', $id_host)
                    ->where('id_room', $id_room)->update(['status' => 'Paid']);
    }
    
    //Lấy ra danh sách các phòng mà user đi tìm phòng có thể thanh toán
    public function getRoomNeedPayment()
    {
        return BookingModel::with('room.image', 'user')->where('id_user', Auth::id())
                                                       ->where('status', 'Accepted')->get()->toArray();
    }
}