<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PaymentModel extends Model
{
    use HasFactory;
    protected $table = "payments";
    protected $fillable =['id_user_payment', 'order_code', 'money', 'payment_content', 'response_code', 'code_vnpay', 'code_bank'];

    public function createPayment($request)
    {
        PaymentModel::create([
            'id_user_payment' => Auth::id(), 
            'order_code' => $request->vnp_TxnRef, 
            'money' => $request->vnp_Amount/100, 
            'payment_content' => $request->vnp_OrderInfo, 
            'response_code' => $request->vnp_ResponseCode, 
            'code_vnpay' => $request->vnp_TransactionNo, 
            'code_bank' => $request->vnp_BankCode,
        ]);
    }
}