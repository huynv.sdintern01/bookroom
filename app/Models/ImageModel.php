<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    use HasFactory;
    protected $table = "images";
    protected $fillable =['id_room', 'url', 'img_type', 'created_at', 'updated_at'];
}