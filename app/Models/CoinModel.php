<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoinModel extends Model
{
    const ADMIN_ID = 3;
    use HasFactory;
    protected $table = "coins";
    protected $fillable = ['id_user', 'coin_number'];

    public function updateCoin($request)
    {
        $coinPayment = $request->vnp_Amount/100;
        $arrOrderInfor = explode("-", $request->vnp_OrderInfo);
        $id_host = array_pop($arrOrderInfor);
        $admin = CoinModel::where('id_user', self::ADMIN_ID)->first()->toArray();
        $host = CoinModel::where('id_user', $id_host)->first()->toArray();
        $coinNumberAdmin = empty($admin) ? 0 : $admin['coin_number'];
        $coinNumberHost = empty($host) ? 0 : $host['coin_number'];
        CoinModel::updateOrCreate(
            ['id_user' => self::ADMIN_ID],
            ['coin_number' =>  $coinNumberAdmin+$coinPayment*10/100] 
        );

        CoinModel::updateOrCreate(
            ['id_user' => $id_host],
            ['coin_number' =>  $coinNumberHost+$coinPayment*90/100] 
        );
    }
}