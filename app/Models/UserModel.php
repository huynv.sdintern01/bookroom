<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class UserModel extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    public const ADMIN = 1;
    public const HOST = 2;
    const TENANT = 3;
    protected $table = "users";
    protected $fillable =['user_name', 'password', 'name', 'gender', 'email', 'province_id', 'town_id', 'village_id', 'phone_number', 'level', 'address', 'age'];
    
    //Relationship
    public function room()
    {
        return $this->hasMany(Room::class, 'user_id', 'id');
    }

    public function booking()
    {
        return $this->hasMany(BookingModel::class, 'id_user', 'id');
    }
    //Xử lý với database
    public function checkLogin($request)
    {
        if(Auth::attempt(['user_name' => $request->username, 'password' => $request->pass, 'level' => self::ADMIN]))
        {
            session(['login_success' => 'Đănh nhập thành công']);
            session(['user_name_admin' => $request->username]);
            return 'admin.index';
        }
        if(Auth::attempt(['user_name' => $request->username, 'password' => $request->pass, 'level' => self::HOST]))
        {
            $user = UserModel::where('user_name', $request->username)->first();
            Auth::login($user);
            return 'host.index';
        }
        if(Auth::attempt(['user_name' => $request->username, 'password' => $request->pass, 'level' => self::TENANT]))
        {
            $user = UserModel::where('user_name', $request->username)->first();
            Auth::login($user);
            return 'tenant.index';
        }
        else{
            session(['err_login' => 'Sai tài khoản hoặc mật khẩu']);
            return "login";
        }
    }

    //Đăng xuất
    public function logOut($request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }

    //Đăng ký tài khoản
    public function insertUser($request)
    {
        UserModel::create([
            'user_name' => $request->userName, 
            'password' => bcrypt($request->pass), 
            'name' => $request->name, 
            'gender' => $request->gender, 
            'email' => $request->email, 
            'phone_number' => $request->phoneNum, 
            'level' => $request->level,
            'age' => $request->age,
            'address' => $request->address,
            'province_id' => $request->province_id,
            'town_id' => $request->town_id, 
            'village_id' => $request->village_id,
        ]);
    }

    //Lấy danh sách user theo level
    public function getListUserWithLevel($request)
    {
        return UserModel::where('level', $request->level)->get()->toArray();
    }
}