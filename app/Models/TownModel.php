<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TownModel extends Model
{
    use HasFactory;
    protected $table = "towns";
    protected $fillable =['code', 'name', 'type', 'code_province_city'];

    //Relationship
    public function village()
    {
        return $this->hasMany(VillageModel::class, 'code_town', 'code');
    }

    public function province_cities()
    {
        return $this->belongsTo(ProvinceCityModel::class, 'code_province_city', 'code');
    }

    public function room()
    {
        return $this->hasManyThrough(RoomModel::class, VillageModel::class, 'code_town', 'code_village', 'code', 'code');
    }

    //Xử lý với database
    public function getCodeTown($request)
    {
        return TownModel::where('code_province_city',$request->id)->pluck('name','code');
    }
}