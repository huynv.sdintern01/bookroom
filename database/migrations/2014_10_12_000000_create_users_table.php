<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigInteger('id', true)->unique()->unsigned();
            $table->string('user_name', 191);
            $table->string('password', 191);
            $table->string('name', 191);
            $table->string('gender', 191);
            $table->string('email', 191)->unique();
            $table->string('province',50)->nullable();
            $table->string('town',50)->nullable();
            $table->string('village',50)->nullable();
            $table->string('age')->nullable();
            $table->tinyInteger('level');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};