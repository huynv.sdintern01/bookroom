<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->bigInteger('id_user_payment')->unsigned();
            $table->string('order_code');
            $table->string('money');
            $table->string('payment_content');
            $table->string('response_code');
            $table->string('code_vnpay');
            $table->string('code_bank');
            $table->dateTime('time')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};