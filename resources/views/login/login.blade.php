<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('login/fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('login/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('login/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('login/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('login/fonts/icomoon/style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Login</title>
    @if (session()->has('sigin_success'))
        <script>
            alert("{{session()->get('sigin_success')}}");
        </script> 
        {{session()->forget('sigin_success')}}
    @else

    @endif

</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-2">
                    <img src="{{asset('login/images/undraw_file_sync_ot38.svg')}}" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Welcome to <strong>My Web</strong></h3>
                                <p class="mb-4">Welcome to my web if you don't have a account please click Create</p>
                            </div>
                            <form action="{{route('start_login')}}" method="POST">
                                @csrf
                                <div class="form-group first">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" name="username" value="{{old('username')}}">
                                    @error('username')
                                         <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="pass">
                                    @error('pass')
                                         <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="d-flex mb-5 align-items-center">
                                    <span style="color:red ;">
                                        
                                    </span>
                                </div>
                                <div class="d-flex mb-5 align-items-center">
                                    <label class="control control--checkbox mb-0">
                                        <span class="caption">Remember me</span>
                                        <input type="checkbox" checked="checked" />
                                        <div class="control__indicator"></div>
                                    </label>
                                    <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span>
                                </div>

                                <input type="submit" value="Log In" class="btn text-white btn-block btn-primary login">
                                @if (session()->has('err_login'))
                                    <p style="color: red;">{{session()->get('err_login')}}</p>
                                    {{session()->forget('err_login')}}
                                @else

                                @endif
                                <span class="d-block text-left my-4 text-muted">
                                    <a href="{{route('sigin')}}" class="sigin">
                                        Create
                                    </a> 
                                    or sign in with
                                </span>

                                <div class="social-login">
                                    <a href="#" class="facebook">
                                        <span class="icon-facebook mr-3"></span>
                                    </a>
                                    <a href="#" class="twitter">
                                        <span class="icon-twitter mr-3"></span>
                                    </a>
                                    <a href="#" class="google">
                                        <span class="icon-google mr-3"></span>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <script src="{{asset('login/js/ajax.js')}}"></script>
    <script src="{{asset('login/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('login/js/popper.min.js')}}"></script>
    <script src="{{asset('login/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('login/js/main.js')}}"></script>
</body>

</html>