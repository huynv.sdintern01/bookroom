@extends("layouts.main")

@section('tittle')
    <title>Trang chủ</title>
@endsection

@section('link')
    @include('components.link')
@endsection

@include('components.alert_message')

@section('menu')
    @include('components.tenant_menu')
@endsection

@section('filter')
    @include('components.form_filter')
@endsection

@section('js')
    @include('components.js')
@endsection