@extends("layouts.main")

@section('tittle')
    <title>Lọc</title>
@endsection

@section('link')
    @include('components.link')
@endsection

@section('menu')
    @include('components.tenant_menu')
@endsection

@section('filter')
    @include('components.form_filter')
@endsection

@section('content')
@include('components.filter_room_with_province')
@endsection

@section('js')
    @include('components.js')
@endsection