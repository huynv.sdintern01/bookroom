<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('login/fonts/icomoon/style.css')}}">
    <link rel="stylesheet" href="{{asset('login/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('login/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('login/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('login/fonts/icomoon/style.css')}}">
    
    <title>Sig In</title>
</head>

<body>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-2">
                    <img src="{{asset('login/images/undraw_file_sync_ot38.svg')}}" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Welcome to <strong>My Web</strong></h3>
                                <p style="color: red;" class="mb-4">To Creating account, you have to:</p>
                                <p style="color: red;">- Write all your information</p>
                                <p style="color: red;">- Phone number and age must be numbers</p>
                                <p style="color: red;">- Definitely Password equal Confirm Password</p>
                            </div>
                            <form action="{{route('start_sigin')}}" method="post">
                                @csrf
                                <div class="form-group first">
                                    <label for="username">Tài khoản:</label>
                                    <input type="text" class="form-control" id="username" name="userName" value="{{old('userName')}}">
                                    @error('userName')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Mật khẩu:</label>
                                    <input type="password" class="form-control" id="password" name="pass" value="{{old('pass')}}">
                                    @error('pass')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Nhập lại mật khẩu:</label>
                                    <input type="password" class="form-control" id="password" name="cfpass" value="{{old('cfpass')}}">
                                    @error('cfpass')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Họ tên:</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                    @error('name')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <span>Giới tính:</span><br>
                                    <select class="form-control" style="width: 100%;" name="gender" id="">
                                        <option value="Nam">Nam</option>
                                        <option value="Nữ">Nữ</option>
                                        <option value="Khác">Khác..</option>
                                    </select>
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Email:</label>
                                    <input type="text" class="form-control" id="" name="email" value="{{old('email')}}">
                                    @error('email')
                                         <span style="color: red;">{{$message}}</span>
                                     @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Số điện thoại:</label>
                                    <input type="text" class="form-control" id="" name="phoneNum" value="{{old('phoneNum')}}">
                                    @error('phoneNum')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Tuổi</label>
                                    <input type="text" class="form-control" id="" name="age" value="{{old('age')}}">
                                    @error('age')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <span>Tỉnh:</span><br>
                                    <select class="province_city form-control" name="province_id" id="select">
                                        <option value="">--Tỉnh/Thành phố--</option>
                                        @foreach ($province as $item)
                                            <option value="{{$item['code']}}">{{$item['name']}}</option>
                                        @endforeach
                                    </select>
                                    @error('province_id')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <span>Huyện:</span><br>
                                    <select class="town form-control" name="town_id" id="">
                                        <option value="">--Huyện--</option>
                                    </select>
                                    @error('town_id')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <span>Xã:</span><br>
                                    <select class="village form-control" name="village_id" id="">
                                        <option value="">--Xã--</option>
                                    </select>
                                    @error('village_id')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <label for="password">Địa chỉ:</label>
                                    <input type="text" class="form-control" id="" name="address" value="{{old('major')}}">
                                    @error('address')
                                        <span style="color: red;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group last mb-4">
                                    <span>Loại tài khoản:</span><br>
                                    <select class="form-control" style="width: 100%;" name="level" id="">
                                        <option value="2">Chủ trọ</option>
                                        <option value="3">Người tìm phòng</option>
                                    </select>
                                </div>
                                <input type="submit" value="Sig In" class="btn text-white btn-block btn-primary">
                                <div class="d-flex mb-5 align-items-center">
                                </div>
                            </form>
                        </div>

                       
                    </div>

                </div>

            </div>
        </div>
    </div>
    <script src="{{asset('login/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('login/public/js/popper.min.js')}}"></script>
    <script src="{{asset('login/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('login/js/main.js')}}"></script>
    <script src="{{asset('login/js/ajax.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('select[name="province_id"]').on('change',function(){
                var code= $(this).val();
                if (code) {
                 $.ajax({
                    url: "{{url('/getTowns/')}}/"+code,
                    type: "GET",
                    dataType: "json",
                    success: function(data){
                        $('select[name="town_id"]').empty();
                        $.each(data,function(key,value){
                            $('select[name="town_id"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                    });
                    }else {
                        $('select[name="town_id"]').empty();
                }
           });
             $('select[name="town_id"]').on('change',function(){
                var code= $(this).val();
                if (code) {
                 $.ajax({
                    url: "{{url('/getVillages/')}}/"+code,
                    type: "GET",
                    dataType: "json",
                    success: function(data){
                        $('select[name="village_id"]').empty();
                        $.each(data,function(key,value){
                            $('select[name="village_id"]').append('<option value="'+key+'">'+value+'</option>');
                        });
                    }
                    });
                }else {
                     $('select[name="village_id"]').empty();
               }
           });
           });
    </script>
</body>

</html>