@extends("layouts.main")

@section('tittle')
    <title>Danh sách phòng đã được đặt</title>
@endsection

@section('link')
    @include('components.link')
@endsection

@section('menu')
    @include('components.host_menu')
@endsection

@section('content')
<section class="ftco-section goto-here">
    <div class="container">
        <div class="col-md-12 heading-section text-center ftco-animate mb-5">
            <span class="subheading">What we do</span>
          <h2 class="mb-2">Danh sách phòng đang được đặt</h2>
        </div>
    <div class="row">
        <table style="width: 100%" class="table">
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Tiêu đề</th>
                <th scope="col">Giá</th>
                <th scope="col">Diện tích</th>
                <th scope="col">Người đặt</th>
                <th scope="col">Số điện thoại người đặt</th>
                <th scope="col">Hành động</th>
              </tr>
            </thead>
            <tbody>
                @foreach($listRoom as $item)
                <tr>
                    <th>{{$item['room']['id']}}</th>
                    <th>{{$item['room']['tittle']}}</th>
                    <th>{{$item['room']['cost']}}</th>
                    <th>{{$item['room']['area']}}</th>
                    <th>{{$item['user']['name']}}</th>
                    <th>{{$item['user']['phone_number']}}</th>
                    <th><a class="btn btn-primary" href="{{route('accept.booking.room', ['id' => $item['id']])}}">Chấp nhận</a></th>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>
    </div>
</section>	
@endsection

@section('js')
    @include('components.js')
@endsection