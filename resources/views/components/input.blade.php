<div class="form-group">
    <span {{$hidden ?? ''}}>{{ucfirst($label)}}:</span><br>
    <input type="{{$type}}" class="form-control" name="{{$name}}" value="{{$value ?? ''}}" placeholder="{{ucfirst($placeHolder)}}">
    @error($name)
        <span style="color: red;">{{$message}}</span>
    @enderror
</div>