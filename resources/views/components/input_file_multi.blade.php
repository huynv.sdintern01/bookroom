<div class="form-group">
    <span>{{ucfirst($label)}}:</span><br>
    <input type="{{$type}}" class="form-control" name="{{$name.'[]'}}" value="{{$value ?? ''}}" class="form-control" {{$multi}}>
    @error($name."*")
        <span style="color: red;">{{$message}}</span>
    @enderror
</div>