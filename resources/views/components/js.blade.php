<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.js" integrity="sha512-9e9rr82F9BPzG81+6UrwWLFj8ZLf59jnuIA/tIf8dEGoQVu7l5qvr02G/BiAabsFOYrIUTMslVN+iDYuszftVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{{asset('ui/js/jquery.min.js')}}"></script>
<script src="{{asset('ui/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{asset('ui/js/popper.min.js')}}"></script>
<script src="{{asset('ui/js/bootstrap.min.js')}}"></script>
<script src="{{asset('ui/js/jquery.easing.1.3.js')}}"></script>
 <script src="{{asset('ui/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('ui/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('ui/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('ui/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('ui/js/aos.js')}}"></script>
<script src="{{asset('ui/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('ui/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('ui/js/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('ui/js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('ui/js/google-map.js')}}"></script>
<script src="{{asset('ui/js/main.js')}}"></script>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<script type="text/javascript">
	Dropzone.options.adddrop =
	{
		maxFilesize: 12,
		renameFile: function(file) {
			var dt = new Date();
			var time = dt.getTime();
			return time+file.name;
		},
		acceptedFiles: ".jpeg,.jpg,.png,.gif",
		addRemoveLinks: true,
		timeout: 5000,
		success: function(file, response) 
		{
			console.log(response);
		},
		error: function(file, response)
		{
			return false;
		}
	};
</script>

<script>
$(document).ready(function(){
	$('select[name="province_id"]').on('change',function(){
		var code= $(this).val();
		if (code) {
			$.ajax({
			url: "{{url('/getTowns/')}}/"+code,
			type: "GET",
			dataType: "json",
			success: function(data){
				$('select[name="town_id"]').empty();
				$.each(data,function(key,value){
					$('select[name="town_id"]').append('<option value="'+key+'">'+value+'</option>');
				});
			}
			});
			}else {
				$('select[name="town_id"]').empty();
		}
	});
		$('select[name="town_id"]').on('change',function(){
		var code= $(this).val();
		if (code) {
			$.ajax({
			url: "{{url('/getVillages/')}}/"+code,
			type: "GET",
			dataType: "json",
			success: function(data){
				$('select[name="village_id"]').empty();
				$.each(data,function(key,value){
					$('select[name="village_id"]').append('<option value="'+key+'">'+value+'</option>');
				});
			}
			});
		}else {
				$('select[name="village_id"]').empty();
		}
	});
	});
</script>

<script>
	paypal.Button.render({
	  // Configure environment
	  env: 'sandbox',
	  client: {
		sandbox: 'AZUDC4-KnTrYO6mLYrKHlOzKP7gjrjG1UtJSnCiE0RAFgKid0QCbmUqkDYdYH5bGmZgdILia2pfV6J-5',
		production: 'demo_production_client_id'
	  },
	  // Customize button (optional)
	  locale: 'en_US',
	  style: {
		size: 'small',
		color: 'gold',
		shape: 'pill',
	  },
  
	  // Enable Pay Now checkout flow (optional)
	  commit: true,
  
	  // Set up a payment
	  payment: function(data, actions) {
		return actions.payment.create({
		  transactions: [{
			amount: {
			  total: '100',
			  currency: 'USD'
			}
		  }]
		});
	  },
	  // Execute the payment
	  onAuthorize: function(data, actions) {
		return actions.payment.execute().then(function() {
		  // Show a confirmation message to the buyer
		  window.alert('Thank you for your purchase!');
		});
	  }
	}, '#paypal-button');
  
  </script>