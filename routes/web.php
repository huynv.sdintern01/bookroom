<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Authenticate;
use App\Http\Controllers\Authentication;
use App\Http\Controllers\CheckOutController;
use App\Http\Controllers\HostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\TenantController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Authentication::class, 'login'])->name('login');
Route::post('/startLogin', [Authentication::class, 'startLogin'])->name('start_login');
Route::get('/sigin', [Authentication::class, 'sigin'])->name('sigin');
Route::post('/startSigin', [Authentication::class, 'startSigin'])->name('start_sigin');
Route::get('/logOut', [Authentication::class, 'logOut'])->name('logout');
Route::get('/getVillages/{id}', [Authentication::class, 'getVillages']);
Route::get('/getTowns/{id}', [Authentication::class, 'getTowns']);


Route::group(['prefix' => 'admins', 'middleware' => 'auth'], function ()
{
    Route::middleware(['user:1', 'auth'])->group(function () {
        Route::get('/', [AdminController::class, 'indexAdmin'])->name('admin.index');
        Route::get('/listHost/{level}', [AdminController::class, 'showListUser'])->name('admin.list.host');
        Route::get('/listTenant/{level}', [AdminController::class, 'showListUser'])->name('admin.list.tenant');
        Route::get('/listRoom', [AdminController::class, 'showLisRoom'])->name('admin.list.room');
    });
});

Route::group(['prefix' => 'tenants', 'middleware' => 'auth'], function ()
{
    Route::middleware(['user:3', 'auth'])->group(function () {
        Route::get('/', [TenantController::class, 'indexTenant'])->name('tenant.index');
        Route::post('/filter', [TenantController::class, 'filterRoom'])->name('filter.room');
        Route::get('/detail/{room_id}', [TenantController::class, 'showDetailRoom'])->name('detail.room');
        Route::get('/booking/{room_id}/{id_host}', [TenantController::class, 'bookingRoom'])->name('booking.room');
        Route::get('/paymentRoom', [TenantController::class, 'showPaymentRoom'])->name('list.payment.room');
        Route::post('/checkOut', [CheckOutController::class, 'payment'])->name('vnpay.payment');
        Route::get('/returnPayment', [TenantController::class, 'returnPay'])->name('return.payment');
        Route::get('/confirmPayment', [TenantController::class, 'confirmPayment'])->name('confirm.payment');
    });
});

Route::group(['prefix' => 'host', 'middleware' => 'auth'], function ()
{
    Route::middleware(['user:2', 'auth'])->group(function () {
        Route::get('/', [HostController::class, 'indexHost'])->name('host.index');
        Route::post('/storePost', [HostController::class, 'storePost'])->name('store.post');
        Route::get('/listBooking', [HostController::class, 'showListBooking'])->name('list.booking.room');
        Route::get('/acceptBooking/{id}', [HostController::class, 'updateAcceptBooking'])->name('accept.booking.room');
        Route::get('/listRoomPost', [HostController::class, 'showListRoom'])->name('list.room.post');
        Route::get('/deleteRoomPost/{id}', [HostController::class, 'destroyRoom'])->name('delete.room.post');
        Route::get('/editRoomPost/{room_id}', [HostController::class, 'editRoom'])->name('edit.room.post');
        Route::put('/updateRoomPost', [HostController::class, 'updateRoom'])->name('update.room.post');
    });
});